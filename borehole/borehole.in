#Description: CSIRO Borehole Scenario

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
    /
  /
END

SUBSURFACE

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    I129
    Cs135
  /
  DATABASE ./ufd-decay.dat
  OUTPUT
    TOTAL
    all
  /
END

#=========================== solver options ===================================
TIMESTEPPER FLOW
END

NEWTON_SOLVER FLOW
  ITOL_UPDATE 1.d0     ! Convergences with max change in pressure is 1 Pa.
END

LINEAR_SOLVER FLOW
END

NEWTON_SOLVER TRANSPORT
END

LINEAR_SOLVER TRANSPORT
END

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED CYLINDRICAL
  NXYZ 142 1 175
  ORIGIN 0.d0 0.d0 -2000.d0
  DXYZ
    # X
    20@0.1 10@0.2 10@0.4 10@0.8 12@2 15@4 40@10 25@20
    # Y
    1.
    # Z
    10.
  /
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY granite
  ID 1
  POROSITY 0.005d0
  TORTUOSITY 1.d0
  ROCK_DENSITY 2500.d0
END

MATERIAL_PROPERTY disposal_zone
  ID 2
  POROSITY 0.005d0
  TORTUOSITY 1.d0
  ROCK_DENSITY 2500.d0
END

MATERIAL_PROPERTY backfill
  ID 3
  POROSITY 0.2d0
  TORTUOSITY 1.d0
  ROCK_DENSITY 2500.d0
END

MATERIAL_PROPERTY fractured_rock
  ID 4
  POROSITY 0.05d0
  TORTUOSITY 1.d0
  ROCK_DENSITY 2500.d0
END

#=========================== output options ===================================
OUTPUT
  SNAPSHOT_FILE
    FORMAT HDF5
    PERIODIC TIME 1 y BETWEEN 0 y AND 10 y
    PERIODIC TIME 10 y BETWEEN 0 y AND 100 y
    PERIODIC TIME 100 y BETWEEN 0 y AND 1000 y
    PERIODIC TIME 1000 y BETWEEN 0 y AND 10000 y
    PERIODIC TIME 10000 y BETWEEN 0 y AND 100000 y
    PERIODIC TIME 100000 y BETWEEN 0 y AND 1000000 y
    VARIABLES
      MATERIAL_ID_KLUDGE_FOR_VISIT
    /
  /
  OBSERVATION_FILE
    PERIODIC TIMESTEP 1
  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 1.d5 y
  INITIAL_TIMESTEP_SIZE 1.d-2 d
  MAXIMUM_TIMESTEP_SIZE 1.d3 y
END

#=========================== regions ==========================================
REGION all
  COORDINATES
    -1.d20 -1.d20 -1.d20
    1.d20 1.d20 1.d20
  /
END

REGION granite
  COORDINATES
    0.3d0 0.d0 -2000.d0
    1000.d0 1.d0 -1000.d0
  /
END

REGION fractured_rock
  COORDINATES
    0.3d0 0.d0 -1000.d0
    1000.d0 1.d0 -250.d0
  /
END

REGION backfill
  COORDINATES
    0.d0 0.d0 -1400.d0
    0.3d0 1.d0 -250.d0
  /
END

REGION disposal_zone
  COORDINATES
    0.d0 0.d0 -2000.d0
    0.3d0 1.d0 -1400.d0
  /
END

REGION east
  FACE EAST
  COORDINATES
    1000.d0 0.d0 -2000.d0 
    1000.d0 1.d0 -250.d0
  /
END

REGION top
  FACE TOP
  COORDINATES
    0.d0 0.d0 -250.d0 
    1000.d0 1.d0 -250.d0
  /
END

REGION obs_pt_in_disposal_zone
  COORDINATE 0.d0 0.d0 -1700.d0
END

REGION obs_pt_in_backfill
  COORDINATE 0.d0 0.d0 -1200.d0
END

REGION obs_pt_in_granite
  COORDINATE 500.d0 0.d0 -1700.d0
END

REGION obs_pt_at_top
  COORDINATE 0.d0 0.d0 -300.d0
END

#=========================== observation points ===============================
OBSERVATION
  REGION obs_pt_in_disposal_zone
END
 
OBSERVATION
  REGION obs_pt_in_backfill
END
 
OBSERVATION
  REGION obs_pt_in_granite
END
 
OBSERVATION
  REGION obs_pt_at_top
END
 
#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE DIRICHLET
  CONSTRAINT initial
    CONCENTRATIONS
      I129  1.d-10 T
      Cs135 1.d-10 T
    /
  /
END

TRANSPORT_CONDITION source
  TYPE DIRICHLET
  CONSTRAINT source
    CONCENTRATIONS
      I129  1.d0   T
      Cs135 1.d0   T
    /
  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION initial
  REGION all
END

INITIAL_CONDITION
  TRANSPORT_CONDITION source
  REGION disposal_zone
END

# top boundary condition
BOUNDARY_CONDITION top
  TRANSPORT_CONDITION initial
  REGION top
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION backfill
  MATERIAL backfill
END

STRATA
  REGION granite
  MATERIAL granite
END

STRATA
  REGION fractured_rock
  MATERIAL fractured_rock
END

STRATA
  REGION disposal_zone
  MATERIAL disposal_zone
END

#========================== checkpointing

END_SUBSURFACE

