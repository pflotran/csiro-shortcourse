import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import pflotran as pft

path = []
path.append('.')

filename = 'borehole-obs-0.tec'

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("I129 Breakthrough",fontsize=16)
plt.xlabel('Time [y]')
plt.ylabel('I129 [M]')

#plt.xlim(0.,1.)
#plt.ylim(0.,1.)
#plt.grid(True)
plt.yscale('log')

data = pft.Dataset(filename,1,2)
plt.plot(data.get_array('x'),data.get_array('y'),label='disposal_zone')
data = pft.Dataset(filename,1,4)
plt.plot(data.get_array('x'),data.get_array('y'),label='backfill')
data = pft.Dataset(filename,1,6)
plt.plot(data.get_array('x'),data.get_array('y'),label='granite')
data = pft.Dataset(filename,1,8)
plt.plot(data.get_array('x'),data.get_array('y'),label='top')

plt.legend(loc=2)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#      plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.gca().get_legend().get_frame().set_fill(False)
plt.gca().get_legend().draw_frame(False)

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

plt.show()
